﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager {
    static class Program {
        public static Controller contrl;
        public static formWanager view;
        public static Authentifier auth;
        public static Model model;

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            auth = new Authentifier();
            contrl = new Controller();
            view = new formWanager();
            Application.Run(view);
        }
    }
}
