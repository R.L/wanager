﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model
{
    class Model
    {
        public BatchsADO Batchs { get; private set; }
        public BarrelsADO Barrels { get; private set; }
        public CellarsADO Cellars { get; private set; }
        public CommandsADO Commands { get; private set; }
        public UsersADO Users { get; private set; }
        public User User { get; private set; }

        public Model(Authentifier nauth)
        {
            User = nauth.User;
            if (User.Batchper)
            {
                this.Batchs = new BatchsADO(nauth);
            }
            if (User.Cellarper)
            {
                this.Cellars = new CellarsADO(nauth);
            }
            if (User.Barrelper)
            {
                this.Barrels = new BarrelsADO(nauth, this.Batchs, this.Cellars);
            }
            if (User.Commandper)
            {
                this.Commands = new CommandsADO(nauth, this.Batchs);
            }
            if (User.Userper)
            {
                this.Users = new UsersADO(nauth);
            }
            
        }
    }
}
