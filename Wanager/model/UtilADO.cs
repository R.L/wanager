﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model
{
    public abstract class UtilADO<T> where T:DBObject
    {
        #region Attributes
        //protected string conexstr;
        protected MySqlConnection conex;
        protected readonly string tbname;
        protected List<T> tbcont;
        protected readonly string insertstr;
        protected readonly string updatestr;
        protected readonly string[] fields;
        
        #endregion

        #region Builders
        protected UtilADO(Authentifier nauth,string ntbname,string[] nfields)
        {
            this.conex = nauth.Conn;
            this.tbname = ntbname;
            this.tbcont = new List<T>();
            this.fields = nfields;
            
            string nupdatestr = "UPDATE FROM "+ntbname+" SET ";
            string ninsertstr = "INSERT INTO " + ntbname + " (";
            string ninsertstr2 = ") VALUES(";

            foreach (string field in nfields)
            {
                nupdatestr += field+"=@"+field+",";
                ninsertstr += field + ",";
                ninsertstr2 += "@" + field + ",";
            }

            nupdatestr.Remove(nupdatestr.Length - 1);
            ninsertstr.Remove(ninsertstr.Length - 1);
            ninsertstr2.Remove(ninsertstr2.Length - 1);

            this.updatestr = nupdatestr + " WHERE id=@id";
            this.insertstr = ninsertstr+ ninsertstr2+")";
        }
        #endregion

        #region Connection
        protected void Open()
        {
            if (conex != null && conex.State == ConnectionState.Closed)
                conex.Open();
        }
        public bool IsOpenned()
        {
            if (conex != null && conex.State == ConnectionState.Open)
                return true;

            return false;
        }
        protected void Close()
        {
            if (conex != null && conex.State == ConnectionState.Open)
                conex.Close();
        }
        #endregion

        #region Request
        protected List<T> DoSelect()
        {
            return this.DoSelect("");
        }

        protected List<T> DoSelect(string extra)
        {
            List<T> rtns = new List<T>();
            try
            {
                Open();
                MySqlCommand cmd = new MySqlCommand(@"SELECT * FROM "+this.tbname+" "+extra, this.conex);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    rtns.Add(this.CreateDBObject(reader));
                }
                reader.Close();
            }
            catch (Exception){}
            finally
            {
                Close();
            }
            return rtns;
        }

        protected bool DoUpdate(T item)
        {
            bool result = true;
            try
            {
                Open();
                MySqlCommand cmd = new MySqlCommand(this.updatestr, this.conex);
                this.BindParamUpdate(cmd, item);
                cmd.Parameters.AddWithValue("@id", item.Id);
                result = 1 == cmd.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                result = false;
            }
            finally
            {
                Close();
            }
            return result;
        }

        protected bool DoDelete(T item) {
            bool result = true;
            try
            {
                Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM " + this.tbname + " WHERE id=@id", this.conex);
                cmd.Parameters.AddWithValue("@id", item.Id);
                result = 1 == cmd.ExecuteNonQuery();
                this.tbcont.Remove(item);
            }
            catch (MySqlException)
            {
                result = false;
            }
            finally
            {
                Close();
            }
            return result;
        }

        protected bool DoInsert(T item)
        {
            bool result = true;
            try
            {
                Open();
                MySqlCommand cmd = new MySqlCommand(this.insertstr, this.conex);
                this.BindParamUpdate(cmd, item);
                result = 1 == cmd.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                result = false;
            }
            finally
            {
                Close();
            }
            return result;
        }

        protected abstract void BindParamUpdate(MySqlCommand cmd, T item);

        protected abstract T CreateDBObject(MySqlDataReader reader);

        #endregion

        #region Util
        public void Refresh()
        {
            this.tbcont = this.DoSelect();
        }

        public List<T> GetItems()
        {
            this.Refresh();
            return this.tbcont;
        }

        public Boolean DeleteItem(T item)
        {
            Boolean rtn = this.DoDelete(item);
            if (rtn)
            {
                this.tbcont.Remove(item);
            }
            else
            {
                this.Refresh();
            }
            return rtn;
        }

        public Boolean UpdateItem(T item)
        {
            Boolean rtn = this.DoUpdate(item);
            if (!rtn)
            {
                this.Refresh();
            }
            return rtn;
        }

        public Boolean InsertItem(T item)
        {
            Boolean rtn = this.DoInsert(item);
            this.Refresh();
            return rtn;
        }

        public T GetById(int id)
        {
            foreach(T item in this.tbcont)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
        #endregion



    }
}
