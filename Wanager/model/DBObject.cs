﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model
{
    public abstract class DBObject {
        public int Id {get;private set;}

        protected DBObject(int nid)
        {
            this.Id = nid;
        } 
    }
}
