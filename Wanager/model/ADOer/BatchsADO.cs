﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Wanager.model;

namespace Wanager.model
{
    public class BatchsADO : UtilADO<Batch>
    {
        public BatchsADO(Authentifier nauth) :base(nauth,"batch",new string[] {"quality","kind","harvest", "bestsale" })
        {
            this.Refresh();
        }

        protected override void BindParamUpdate(MySqlCommand cmd, Batch item)
        {
            cmd.Parameters.AddWithValue("@quality", item.Quality);
            cmd.Parameters.AddWithValue("@kind", item.Kind);
            cmd.Parameters.AddWithValue("@harvest", item.Harvest);
            cmd.Parameters.AddWithValue("@bestsale", item.BestSale);
        }

        protected override Batch CreateDBObject(MySqlDataReader rdr)
        {
            return new Batch(rdr.GetInt32(0), rdr.GetInt32(1), rdr.GetString(2), rdr.GetDateTime(3), rdr.GetDateTime(4));
        }
    }
}
