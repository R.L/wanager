﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Wanager.model
{
    public class BarrelsADO :UtilADO<Barrel>
    {
        protected BatchsADO batchs;
        protected CellarsADO cellars;

         public BarrelsADO(Authentifier nauth, BatchsADO nbatchs, CellarsADO ncellars) :base(nauth,"barrel", new string[]{"cave_id","pos","batch_id","max","filling"})
        {
            this.batchs = nbatchs;
            this.cellars = ncellars;
            this.Refresh();
        }

        protected override Barrel CreateDBObject(MySqlDataReader rdr)
        {
            Barrel rtn = new Barrel(rdr.GetInt32(0), (Cellar)cellars.GetById(rdr.GetInt32(1)), rdr.GetInt32(2), (Batch)batchs.GetById(rdr.GetInt32(3)), rdr.GetInt32(4), rdr.GetInt32(5));
            return rtn;
        }

        protected override void BindParamUpdate(MySqlCommand cmd, Barrel item)
        {
            cmd.Parameters.AddWithValue("@cave_id", item.Cellar.Id);
            cmd.Parameters.AddWithValue("@pos", item.Pos);
            cmd.Parameters.AddWithValue("@batch_id", item.Batch.Id);
            cmd.Parameters.AddWithValue("@max", item.Max);
            cmd.Parameters.AddWithValue("@filling", item.Fill);
        }

        public List<Barrel> GetBarrelsByCellar(Cellar cellar)
        {
            return this.DoSelect("WHERE cellar_id="+cellar.Id);
        }

        public List<Barrel> GetBarrelsByBatch(Batch batch)
        {
            return this.DoSelect("WHERE batch_id=" + batch.Id);
        }
    }
}
