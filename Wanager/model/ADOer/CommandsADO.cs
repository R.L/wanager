﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Wanager.model;

namespace Wanager.model
{
    public class CommandsADO : UtilADO<Command>
    {
        protected BatchsADO batchs;

        public CommandsADO(Authentifier nauth, BatchsADO nbatchs) : base(nauth,"command",new string[]{ "batch_id", "price", "sold", "date","name","pname" })
        {
            this.batchs = nbatchs;
            this.Refresh();
        }

        protected override void BindParamUpdate(MySqlCommand cmd, Command item)
        {
            cmd.Parameters.AddWithValue("@batch_id", item.Batch.Id);
            cmd.Parameters.AddWithValue("@price", item.Price);
            cmd.Parameters.AddWithValue("@sold", item.Sold);
            cmd.Parameters.AddWithValue("@date", item.Date);
            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@pname", item.PName);
        }

        protected override Command CreateDBObject(MySqlDataReader rdr)
        {
            return new Command(rdr.GetInt32(0), (Batch)batchs.GetById(rdr.GetInt32(1)), rdr.GetInt32(2), rdr.GetDateTime(3), rdr.GetDateTime(4),rdr.GetString(5), rdr.GetString(6));
        }
    }
}
