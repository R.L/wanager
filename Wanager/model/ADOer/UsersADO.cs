﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Wanager.model
{
    public class UsersADO : UtilADO<User>
    {
        public UsersADO(Authentifier nauth) : base(nauth, "user", new string[] { "name", "password", "barrel_per", "batch_per", "cellar_per", "command_per", "user_per" })
        {
            this.Refresh();
        }

        protected override void BindParamUpdate(MySqlCommand cmd, User item)
        {
            cmd.Parameters.AddWithValue("@name",item.Name);
            cmd.Parameters.AddWithValue("@password", item.Password);

            cmd.Parameters.AddWithValue("@barrel_per", item.Barrelper);
            cmd.Parameters.AddWithValue("@batch_per", item.Batchper);
            cmd.Parameters.AddWithValue("@cellar_per", item.Cellarper);
            cmd.Parameters.AddWithValue("@command_per", item.Commandper);
            cmd.Parameters.AddWithValue("@user_per", item.Userper);
        }

        protected override User CreateDBObject(MySqlDataReader rdr)
        {
            return new User(rdr.GetInt32(0),rdr.GetString(1),rdr.GetString(2),rdr.GetBoolean(3), rdr.GetBoolean(4), rdr.GetBoolean(5), rdr.GetBoolean(6), rdr.GetBoolean(7));
        }

        public User GetByNameAndPAssword(string name, string password)
        {
            List <User> users = this.DoSelect("WHERE name='" + name + "' AND password='"+ password+"'");
            if (users.Count > 0)
            {
                return users[0];
            }
            return null;
        }
    }
}
