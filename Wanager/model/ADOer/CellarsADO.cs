﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Wanager.model
{
    public class CellarsADO : UtilADO<Cellar>
    {
        public CellarsADO(Authentifier nauth) : base(nauth,"cellar", new string[] { "name","max","state" })
        {
            this.Refresh();
        }

        protected override void BindParamUpdate(MySqlCommand cmd, Cellar item)
        {
            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@max", item.Max);
            cmd.Parameters.AddWithValue("@state", item.State);
        }

        protected override Cellar CreateDBObject(MySqlDataReader rdr)
        {
            return new Cellar(rdr.GetInt32(0), rdr.GetString(1), rdr.GetInt32(2), rdr.GetString(3));
        }
    }
}
