﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model
{
    public class Authentifier
    {
        public MySqlConnection Conn { get; private set; }
        public User User { get; private set; }

        public Authentifier()
        {
            this.Conn = null;
        }
        public string Login(string nname, string npassword)
        {
            string constr = "Server=localhost;Database=wanager;userid=root;password=;CharSet=utf8";
            string rtn = "Une Erreur est survenue";
            Console.WriteLine(constr);
            MySqlConnection conn =null;
            try
            {
                conn = new MySqlConnection(constr);
                conn.Open();
                rtn = "";
                this.Conn = conn;
                UsersADO users = new UsersADO(this);
                this.User = users.GetByNameAndPAssword(nname, npassword);
                if (this.User==null)
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                rtn = "Identifiants invalides";
            }
            catch (MySqlException)
            {
                rtn = "Une erreur serveur est survenue";
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return rtn;
        }
    }
}
