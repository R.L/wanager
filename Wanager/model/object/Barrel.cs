﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model {
    public class Barrel : DBObject
    {
        public Cellar Cellar { get; private set; }
        public Batch Batch { get; private set; }
        public int Pos { get; private set; } 
        public int Max { get; private set; }
        public int Fill { get; private set; }

        public Barrel(int nid, Cellar ncellar, int ncavepos, Batch nbatch, int ncontmax, int ncontcur):base(nid)
        {
            this.Cellar = ncellar;
            this.Pos = ncavepos;
            this.Batch = nbatch;
            this.Max = ncontmax;
            this.Fill = ncontcur;
        }

        
    }
}
