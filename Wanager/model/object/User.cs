﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model
{
    public class User : DBObject
    {
        public string Name { get; private set; }
        public string Password { get; private set; }
        public bool Barrelper { get; private set; }
        public bool Batchper { get; private set; }
        public bool Cellarper { get; private set; }
        public bool Commandper { get; private set; }
        public bool Userper { get; private set; }

        public User(int id, string nname, string npassword, bool nbarrelp, bool nbatchp, bool ncellarp, bool ncommandp, bool nuserp) : base(id)
        {
            this.Name = nname;
            this.Password = npassword;
            this.Barrelper = nbarrelp;
            this.Batchper = nbatchp;
            this.Cellarper = ncellarp;
            this.Commandper = ncommandp;
            this.Userper = nuserp;
        }
    }
}
