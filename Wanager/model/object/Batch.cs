﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Wanager.model
{
    public class Batch:DBObject
    {
        public int Quality { get; private set; }
        public string Kind { get; private set; }
        public DateTime Harvest { get; private set; }
        public DateTime BestSale { get; private set; }

        public Batch(int nid,int nqual,string nkind,DateTime nharv, DateTime nbsale) : base(nid)
        {
            this.Quality = nqual;
            this.Kind = nkind;
            this.Harvest = nharv;
            this.BestSale = nbsale;
        }
    }
}
