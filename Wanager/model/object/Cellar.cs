﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wanager.model;

namespace Wanager.model
{
    public class Cellar : DBObject
    {
        public string Name { get; private set; }
        public int Max { get; private set; }
        public string State { get; private set; }

        public Cellar(int nid,string nname,int nmax, string nstate) : base(nid)
        {
            this.Name = nname;
            this.Max = nmax;
            this.State = nstate;
        }
    }
}
