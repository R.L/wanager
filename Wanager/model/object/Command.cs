﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wanager.model {
    public class Command : DBObject {
        public Batch Batch { get; private set; }
        
        public int Price { get; private set; }
        public DateTime Sold { get; private set; }
        public DateTime Date { get; private set; }
        public string Name { get; private set; }
        public string PName { get; private set; }

        public Command(int nid, Batch nbatch,int nprice,DateTime nsold, DateTime ndate, string nname, string npname) : base(nid)
        {
            this.Batch = nbatch;
            this.Price = nprice;
            this.Sold = nsold;
            this.Date = ndate;
            this.Name = nname;
            this.PName = npname;
        }



    }
}
