﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views.Managers
{
    public class CommandManager : GenericManager<Command>
    {
        public CommandManager(CommandsADO db, IManagerView list) : base(db, list)
        {

        }

        protected override IBar<Command> NewBarSub()
        {
            return new CommandBar();
        }
    }
}
