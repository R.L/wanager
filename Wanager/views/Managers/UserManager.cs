﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wanager.model;

namespace Wanager.views
{
    public class UserManager : GenericManager<User>
    {
        public UserManager(UsersADO db, IManagerView list) : base(db, list)
        {

        }

        protected override IBar<User> NewBarSub()
        {
            return new UserBar();
        }
    }
}
