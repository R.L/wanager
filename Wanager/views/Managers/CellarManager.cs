﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views.Managers
{
    public class CellarManager : GenericManager<Cellar>
    {
        public CellarManager(CellarsADO db, IManagerView list) : base(db, list)
        {

        }

        protected override IBar<Cellar> NewBarSub()
        {
            return new CellarBar();
        }
    }
}
