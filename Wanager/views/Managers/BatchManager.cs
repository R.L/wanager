﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views.Managers
{
    public class BatchManager : GenericManager<Batch>
    {
        public BatchManager(BatchsADO db, IManagerView list) : base(db, list)
        {

        }

        protected override IBar<Batch> NewBarSub()
        {
            return new BatchBar();
        }
    }
}
