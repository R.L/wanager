﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views.Managers
{
    public class BarrelManager : GenericManager<Barrel>
    {
        public BarrelManager(BarrelsADO db, IManagerView list) : base(db, list)
        {

        }

        protected override IBar<Barrel> NewBarSub()
        {
            return new BarrelBar();
        }


    }
}
