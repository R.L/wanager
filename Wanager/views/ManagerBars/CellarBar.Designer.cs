﻿namespace Wanager.views
{
    partial class CellarBar
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelfill = new System.Windows.Forms.Label();
            this.btndelete = new System.Windows.Forms.Button();
            this.labelstate = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labeltitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelfill
            // 
            this.labelfill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelfill.AutoSize = true;
            this.labelfill.Location = new System.Drawing.Point(286, 13);
            this.labelfill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelfill.Name = "labelfill";
            this.labelfill.Size = new System.Drawing.Size(58, 13);
            this.labelfill.TabIndex = 9;
            this.labelfill.Text = "quant/max";
            // 
            // btndelete
            // 
            this.btndelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btndelete.Location = new System.Drawing.Point(346, 5);
            this.btndelete.Margin = new System.Windows.Forms.Padding(2);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(28, 29);
            this.btndelete.TabIndex = 8;
            this.btndelete.Text = "X";
            this.btndelete.UseVisualStyleBackColor = true;
            // 
            // labelstate
            // 
            this.labelstate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelstate.AutoSize = true;
            this.labelstate.Location = new System.Drawing.Point(115, 13);
            this.labelstate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelstate.Name = "labelstate";
            this.labelstate.Size = new System.Drawing.Size(26, 13);
            this.labelstate.TabIndex = 7;
            this.labelstate.Text = "Etat";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelname.AutoSize = true;
            this.labelname.Location = new System.Drawing.Point(45, 13);
            this.labelname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(27, 13);
            this.labelname.TabIndex = 6;
            this.labelname.Text = "nom";
            // 
            // labeltitle
            // 
            this.labeltitle.AutoSize = true;
            this.labeltitle.Location = new System.Drawing.Point(2, 4);
            this.labeltitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(40, 13);
            this.labeltitle.TabIndex = 5;
            this.labeltitle.Text = "N° ___";
            // 
            // CellarBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelfill);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.labelstate);
            this.Controls.Add(this.labelname);
            this.Controls.Add(this.labeltitle);
            this.Name = "CellarBar";
            this.Size = new System.Drawing.Size(377, 36);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelfill;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Label labelstate;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labeltitle;
    }
}
