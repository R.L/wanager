﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public partial class CellarBar : UserControl ,IBar<Cellar>
    {
        protected GenericManager<Cellar> manager;

        public Cellar Item { get; protected set; }

        public CellarBar()
        {
            InitializeComponent();
            this.Visible = true;
            this.Size = new System.Drawing.Size(505, 47);
        }

        public void Select(object sender, EventArgs e)
        {
            this.manager.SetSelection(this);
        }

        public void Delete(object sender, EventArgs e)
        {
            this.manager.DeleteItem(this.Item);
        }

        public Cellar GetItem()
        {
            return this.Item;
        }



        public void SetManager(GenericManager<Cellar> man)
        {
            this.manager = man;
        }

        public UserControl getUC()
        {
            return this;
        }

        public void SetItem(Cellar obj)
        {
            this.Item = obj;
            this.labeltitle.Text = "N° " + obj.Id;
            this.labelname.Text = obj.Name;
            this.labelstate.Text = obj.State;
            this.labelfill.Text = Program.model.Barrels.GetBarrelsByCellar(this.Item).Count()+" fûts/"+obj.Max;
        }
    }
}
