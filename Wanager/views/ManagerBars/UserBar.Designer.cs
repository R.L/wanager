﻿using System.Windows.Forms;

namespace Wanager.views
{
    partial class UserBar
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelfill = new System.Windows.Forms.Label();
            this.btndelete = new System.Windows.Forms.Button();
            this.labeltype = new System.Windows.Forms.Label();
            this.labelyear = new System.Windows.Forms.Label();
            this.labeltitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelfill
            // 
            this.labelfill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelfill.AutoSize = true;
            this.labelfill.Location = new System.Drawing.Point(286, 11);
            this.labelfill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelfill.Name = "labelfill";
            this.labelfill.Size = new System.Drawing.Size(30, 13);
            this.labelfill.TabIndex = 9;
            this.labelfill.Text = "3/60";
            // 
            // btndelete
            // 
            this.btndelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btndelete.Location = new System.Drawing.Point(346, 3);
            this.btndelete.Margin = new System.Windows.Forms.Padding(2);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(28, 29);
            this.btndelete.TabIndex = 8;
            this.btndelete.Text = "X";
            this.btndelete.UseVisualStyleBackColor = true;
            // 
            // labeltype
            // 
            this.labeltype.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labeltype.AutoSize = true;
            this.labeltype.Location = new System.Drawing.Point(115, 11);
            this.labeltype.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltype.Name = "labeltype";
            this.labeltype.Size = new System.Drawing.Size(27, 13);
            this.labeltype.TabIndex = 7;
            this.labeltype.Text = "type";
            // 
            // labelyear
            // 
            this.labelyear.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelyear.AutoSize = true;
            this.labelyear.Location = new System.Drawing.Point(45, 11);
            this.labelyear.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelyear.Name = "labelyear";
            this.labelyear.Size = new System.Drawing.Size(27, 13);
            this.labelyear.TabIndex = 6;
            this.labelyear.Text = "year";
            // 
            // labeltitle
            // 
            this.labeltitle.AutoSize = true;
            this.labeltitle.Location = new System.Drawing.Point(2, 5);
            this.labeltitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(40, 13);
            this.labeltitle.TabIndex = 5;
            this.labeltitle.Text = "N° ___";
            // 
            // UserBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelfill);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.labeltype);
            this.Controls.Add(this.labelyear);
            this.Controls.Add(this.labeltitle);
            this.Name = "UserBar";
            this.Size = new System.Drawing.Size(376, 37);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelfill;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Label labeltype;
        private System.Windows.Forms.Label labelyear;
        private System.Windows.Forms.Label labeltitle;
    }
}
