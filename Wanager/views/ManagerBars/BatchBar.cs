﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public partial class BatchBar : UserControl, IBar<Batch>
    {
        protected GenericManager<Batch> manager;

        public Batch Item { get; protected set; }

        public BatchBar()
        {
            InitializeComponent();
            this.Visible = true;
            this.Size = new System.Drawing.Size(505, 47);
        }

        public void Select(object sender, EventArgs e)
        {
            this.manager.SetSelection(this);
        }

        public void Delete(object sender, EventArgs e)
        {
            this.manager.DeleteItem(this.Item);
        }

        public Batch GetItem()
        {
            return this.Item;
        }

        public void SetManager(GenericManager<Batch> man)
        {
            this.manager = man;
        }

        public UserControl getUC()
        {
            return this;
        }

        public void SetItem(Batch obj)
        {
            this.Item = obj;
            this.labeltitle.Text = "N° " + obj.Id;
            this.labelyear.Text = "" + obj.Harvest;
            this.labeltype.Text = "" + obj.Kind;
            int countfill = 0;
            int countbarrel = 0;
            foreach (Barrel ba in Program.model.Barrels.GetBarrelsByBatch(this.Item))
            {
                countfill += ba.Fill;
                countbarrel++;
            }
            this.labelfill.Text = countfill + "L " + countbarrel + " fûts";
        }
    }
}
