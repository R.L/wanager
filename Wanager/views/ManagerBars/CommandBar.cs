﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public partial class CommandBar : UserControl ,IBar<Command>
    {
        protected GenericManager<Command> manager;

        public Command Item { get; protected set; }

        public CommandBar()
        {
            InitializeComponent();
            this.Visible = true;
            this.Size = new System.Drawing.Size(505, 47);
        }

        public void Select(object sender, EventArgs e)
        {
            this.manager.SetSelection(this);
        }

        public void Delete(object sender, EventArgs e)
        {
            this.manager.DeleteItem(this.Item);
        }

        public Command GetItem()
        {
            return this.Item;
        }

        public void SetManager(GenericManager<Command> man)
        {
            this.manager = man;
        }

        public UserControl getUC()
        {
            return this;
        }

        public void SetItem(Command obj)
        {
            this.Item = obj;
            this.labeltitle.Text = "N° " + obj.Id;
            this.labelprice.Text = obj.Price + "€";
            this.labelname.Text = obj.Name +" "+ obj.PName;
            this.labelkind.Text = "N°"+obj.Batch.Id+" "+obj.Batch.Kind;
        }
    }
}
