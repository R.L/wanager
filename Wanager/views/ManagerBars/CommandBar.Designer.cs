﻿namespace Wanager.views
{
    partial class CommandBar
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelkind = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labelprice = new System.Windows.Forms.Label();
            this.labeltitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelkind
            // 
            this.labelkind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelkind.AutoSize = true;
            this.labelkind.Location = new System.Drawing.Point(278, 13);
            this.labelkind.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelkind.Name = "labelkind";
            this.labelkind.Size = new System.Drawing.Size(27, 13);
            this.labelkind.TabIndex = 9;
            this.labelkind.Text = "type";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelname.AutoSize = true;
            this.labelname.Location = new System.Drawing.Point(121, 13);
            this.labelname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(65, 13);
            this.labelname.TabIndex = 7;
            this.labelname.Text = "nom prenom";
            // 
            // labelprice
            // 
            this.labelprice.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelprice.AutoSize = true;
            this.labelprice.Location = new System.Drawing.Point(45, 13);
            this.labelprice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelprice.Name = "labelprice";
            this.labelprice.Size = new System.Drawing.Size(23, 13);
            this.labelprice.TabIndex = 6;
            this.labelprice.Text = "prix";
            // 
            // labeltitle
            // 
            this.labeltitle.AutoSize = true;
            this.labeltitle.Location = new System.Drawing.Point(2, 4);
            this.labeltitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(40, 13);
            this.labeltitle.TabIndex = 5;
            this.labeltitle.Text = "N° ___";
            // 
            // CommandBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelkind);
            this.Controls.Add(this.labelname);
            this.Controls.Add(this.labelprice);
            this.Controls.Add(this.labeltitle);
            this.Name = "CommandBar";
            this.Size = new System.Drawing.Size(377, 36);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelkind;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelprice;
        private System.Windows.Forms.Label labeltitle;
    }
}
