﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public partial class UserBar : UserControl, IBar<User>
    {
        protected GenericManager<User> manager;

        public User Item { get; protected set; }

        public UserBar()
        {
            InitializeComponent();
            this.Visible = true;
            this.Size = new System.Drawing.Size(505, 47);
        }

        public void Select(object sender, EventArgs e)
        {
            this.manager.SetSelection(this);
        }

        public void Delete(object sender, EventArgs e)
        {
            this.manager.DeleteItem(this.Item);
        }

        public User GetItem()
        {
            return this.Item;
        }



        public void SetManager(GenericManager<User> man)
        {
            this.manager = man;
        }

        public UserControl getUC()
        {
            return this;
        }

        public void SetItem(User obj)
        {
            this.Item = obj;
            this.labeltitle.Text = "N° " + obj.Id;
            
        }
    }
}
