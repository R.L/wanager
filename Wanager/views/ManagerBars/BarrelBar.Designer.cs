﻿namespace Wanager.views
{
    partial class BarrelBar
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeltitle = new System.Windows.Forms.Label();
            this.labelyear = new System.Windows.Forms.Label();
            this.labeltype = new System.Windows.Forms.Label();
            this.btndelete = new System.Windows.Forms.Button();
            this.labelfill = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labeltitle
            // 
            this.labeltitle.AutoSize = true;
            this.labeltitle.Location = new System.Drawing.Point(3, 3);
            this.labeltitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(40, 13);
            this.labeltitle.TabIndex = 0;
            this.labeltitle.Text = "N° ___";
            // 
            // labelyear
            // 
            this.labelyear.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelyear.AutoSize = true;
            this.labelyear.Location = new System.Drawing.Point(46, 13);
            this.labelyear.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelyear.Name = "labelyear";
            this.labelyear.Size = new System.Drawing.Size(27, 13);
            this.labelyear.TabIndex = 1;
            this.labelyear.Text = "year";
            // 
            // labeltype
            // 
            this.labeltype.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labeltype.AutoSize = true;
            this.labeltype.Location = new System.Drawing.Point(116, 13);
            this.labeltype.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeltype.Name = "labeltype";
            this.labeltype.Size = new System.Drawing.Size(27, 13);
            this.labeltype.TabIndex = 2;
            this.labeltype.Text = "type";

            // 
            // btndelete
            // 
            this.btndelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btndelete.Location = new System.Drawing.Point(349, 5);
            this.btndelete.Margin = new System.Windows.Forms.Padding(2);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(28, 29);
            this.btndelete.TabIndex = 3;
            this.btndelete.Text = "X";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.Delete);
            // 
            // labelfill
            // 
            this.labelfill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelfill.AutoSize = true;
            this.labelfill.Location = new System.Drawing.Point(287, 13);
            this.labelfill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelfill.Name = "labelfill";
            this.labelfill.Size = new System.Drawing.Size(30, 13);
            this.labelfill.TabIndex = 4;
            this.labelfill.Text = "3/60";
            // 
            // BarrelBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelfill);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.labeltype);
            this.Controls.Add(this.labelyear);
            this.Controls.Add(this.labeltitle);
            this.Name = "BarrelBar";
            this.Size = new System.Drawing.Size(379, 38);
            this.Click += new System.EventHandler(this.Select);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Label labelyear;
        private System.Windows.Forms.Label labeltype;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Label labelfill;
    }
}
