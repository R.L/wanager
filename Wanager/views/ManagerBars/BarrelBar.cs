﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;
using Wanager.views.Managers;

namespace Wanager.views
{
    public partial class BarrelBar : UserControl, IBar<Barrel>
    {
        protected GenericManager<Barrel> manager;

        public Barrel Item { get; protected set; }

        public BarrelBar()
        {
            InitializeComponent();
            this.Visible = true;
            this.Size = new System.Drawing.Size(505, 47);
        }

        public void Select(object sender, EventArgs e)
        {
            this.manager.SetSelection(this);
        }

        public void Delete(object sender, EventArgs e)
        {
            this.manager.DeleteItem(this.Item);
        }

        public Barrel GetItem()
        {
            return this.Item;
        }

        

        public void SetManager(GenericManager<Barrel> man)
        {
            this.manager = man;
        }

        public UserControl getUC()
        {
            return this;
        }

        public void SetItem(Barrel obj)
        {
            this.Item = obj;
            this.labeltitle.Text = "N° " + obj.Id;
            this.labeltype.Text = "" + obj.Batch.Kind;
            this.labelyear.Text = "" + obj.Batch.Harvest.ToShortDateString();
            this.labelfill.Text = obj.Fill + "/" + obj.Max;
        }
    }
}
