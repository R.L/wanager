﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager;

namespace Wanager.views {
    public partial class Loged : UserControl {
        public Loged() {
            InitializeComponent();
        }

        public void LoadManagers()
        {
            if (Program.model.User.Barrelper)
                this.barrelManagement1.LoadDb();
            if (Program.model.User.Batchper)
                this.batchManagement1.LoadDb();
            if (Program.model.User.Cellarper)
                this.cellarManagement1.LoadDb();
            if (Program.model.User.Commandper)
                this.cmdManagement1.LoadDb();
            //if (Program.model.User.Userper)
                //this.usrManagement1.LoadDb();
        }
        
    }
}
