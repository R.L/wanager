﻿namespace Wanager.views
{
    partial class Login
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginusername = new System.Windows.Forms.TextBox();
            this.loginpassword = new System.Windows.Forms.TextBox();
            this.loginusernamelabel = new System.Windows.Forms.Label();
            this.loginpasswordlabel = new System.Windows.Forms.Label();
            this.loginvalidbutton = new System.Windows.Forms.Button();
            this.labelerror = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginusername
            // 
            this.loginusername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginusername.Location = new System.Drawing.Point(37, 114);
            this.loginusername.Name = "loginusername";
            this.loginusername.Size = new System.Drawing.Size(164, 20);
            this.loginusername.TabIndex = 0;
            this.loginusername.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // loginpassword
            // 
            this.loginpassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginpassword.Location = new System.Drawing.Point(37, 153);
            this.loginpassword.Name = "loginpassword";
            this.loginpassword.PasswordChar = '*';
            this.loginpassword.Size = new System.Drawing.Size(164, 20);
            this.loginpassword.TabIndex = 1;
            // 
            // loginusernamelabel
            // 
            this.loginusernamelabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginusernamelabel.Location = new System.Drawing.Point(33, 98);
            this.loginusernamelabel.Name = "loginusernamelabel";
            this.loginusernamelabel.Size = new System.Drawing.Size(29, 13);
            this.loginusernamelabel.TabIndex = 3;
            this.loginusernamelabel.Text = "Nom";
            this.loginusernamelabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // loginpasswordlabel
            // 
            this.loginpasswordlabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginpasswordlabel.Location = new System.Drawing.Point(33, 137);
            this.loginpasswordlabel.Name = "loginpasswordlabel";
            this.loginpasswordlabel.Size = new System.Drawing.Size(71, 13);
            this.loginpasswordlabel.TabIndex = 4;
            this.loginpasswordlabel.Text = "Mot de passe";
            // 
            // loginvalidbutton
            // 
            this.loginvalidbutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginvalidbutton.Location = new System.Drawing.Point(150, 179);
            this.loginvalidbutton.Name = "loginvalidbutton";
            this.loginvalidbutton.Size = new System.Drawing.Size(52, 23);
            this.loginvalidbutton.TabIndex = 5;
            this.loginvalidbutton.Text = "Valider";
            this.loginvalidbutton.UseVisualStyleBackColor = true;
            this.loginvalidbutton.Click += new System.EventHandler(this.loginvalidbutton_Click);
            // 
            // labelerror
            // 
            this.labelerror.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelerror.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelerror.ForeColor = System.Drawing.Color.Green;
            this.labelerror.Location = new System.Drawing.Point(21, 68);
            this.labelerror.Name = "labelerror";
            this.labelerror.Size = new System.Drawing.Size(199, 17);
            this.labelerror.TabIndex = 6;
            this.labelerror.Text = "Veuillez entrez vos identifiants";
            this.labelerror.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Indigo;
            this.label2.Location = new System.Drawing.Point(1, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 46);
            this.label2.TabIndex = 7;
            this.label2.Text = "WANAGER";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelerror);
            this.Controls.Add(this.loginvalidbutton);
            this.Controls.Add(this.loginpasswordlabel);
            this.Controls.Add(this.loginusernamelabel);
            this.Controls.Add(this.loginpassword);
            this.Controls.Add(this.loginusername);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(240, 215);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox loginusername;
        private System.Windows.Forms.TextBox loginpassword;
        private System.Windows.Forms.Label loginusernamelabel;
        private System.Windows.Forms.Label loginpasswordlabel;
        private System.Windows.Forms.Button loginvalidbutton;
        private System.Windows.Forms.Label labelerror;
        private System.Windows.Forms.Label label2;
    }
}
