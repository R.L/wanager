﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public abstract class GenericManager<Dbobj> //
        where Dbobj : DBObject //
    {
        public IBar<Dbobj> Selected { get; protected set;}
        protected UtilADO<Dbobj> db;
        protected List<IBar<Dbobj>> bars;
        protected IManagerView view;

        public GenericManager(UtilADO<Dbobj> ndb, IManagerView nview)
        {
            this.view = nview;
            this.bars = new List<IBar<Dbobj>>();
            this.db = ndb;
            this.LoadItems();
        }

        protected abstract IBar<Dbobj> NewBarSub();

        public IBar<Dbobj> NewBar(Dbobj obj)
        {
            IBar<Dbobj> rtn = this.NewBarSub();
            rtn.SetManager(this);
            rtn.SetItem(obj);
            return rtn;
        }

        public void SetSelection(IBar<Dbobj> obj)
        {
            if (this.Selected!=null)
            {
                this.Selected.getUC().BackColor = Color.Gray;
            }
            obj.getUC().BackColor = Color.LightGray;
            this.Selected = obj;
        }

        public void AddToDb(Dbobj obj)
        {
            this.db.InsertItem(obj);
            this.LoadItems();
        }

        public void UpdToDb(Dbobj obj)
        {
            this.db.UpdateItem(obj);
            this.LoadItems();
        }

        public void LoadItems()
        {
            List<Dbobj> items = this.db.GetItems();
            List<IBar<Dbobj>> nbars = new List<IBar<Dbobj>>();
            List<UserControl> ucs = new List<UserControl>();
            foreach (Dbobj obj in items)
            {
                IBar<Dbobj> bar = this.NewBar(obj);
                bar.getUC().TabIndex = 0;
                bar.getUC().Location = new System.Drawing.Point(15, nbars.Count * 50);
                nbars.Add(bar);
                ucs.Add(bar.getUC());
            }
            if (this.Selected != null)
            {
                this.SetSelection(nbars[this.Selected.GetItem().Id]);
            }
            this.bars.Clear();
            this.bars.AddRange(nbars);
            this.view.LoadItems(ucs);
            this.view.LoadLinks();
        }

        public void DeleteItem(Dbobj obj)
        {
            this.db.DeleteItem(obj);
        }
    }
}
