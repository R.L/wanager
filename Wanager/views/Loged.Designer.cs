﻿namespace Wanager.views {
    partial class Loged {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.barrelManagement1 = new Wanager.views.BarrelManagement();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmdManagement1 = new Wanager.views.CmdManagement();
            this.Récoltes = new System.Windows.Forms.TabPage();
            this.cellarManagement1 = new Wanager.views.CellarManagement();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.batchManagement1 = new Wanager.views.BatchManagement();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.userManagement1 = new Wanager.views.OldManagers.UserManagement();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.Récoltes.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.Récoltes);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(487, 602);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.barrelManagement1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(479, 576);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Futs";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // barrelManagement1
            // 
            this.barrelManagement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.barrelManagement1.Location = new System.Drawing.Point(2, 2);
            this.barrelManagement1.Margin = new System.Windows.Forms.Padding(0);
            this.barrelManagement1.Name = "barrelManagement1";
            this.barrelManagement1.Padding = new System.Windows.Forms.Padding(3);
            this.barrelManagement1.Size = new System.Drawing.Size(475, 572);
            this.barrelManagement1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cmdManagement1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(479, 576);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Commandes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cmdManagement1
            // 
            this.cmdManagement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdManagement1.Location = new System.Drawing.Point(3, 3);
            this.cmdManagement1.Name = "cmdManagement1";
            this.cmdManagement1.Size = new System.Drawing.Size(473, 570);
            this.cmdManagement1.TabIndex = 0;
            // 
            // Récoltes
            // 
            this.Récoltes.Controls.Add(this.cellarManagement1);
            this.Récoltes.Location = new System.Drawing.Point(4, 22);
            this.Récoltes.Name = "Récoltes";
            this.Récoltes.Padding = new System.Windows.Forms.Padding(3);
            this.Récoltes.Size = new System.Drawing.Size(479, 576);
            this.Récoltes.TabIndex = 2;
            this.Récoltes.Text = "Caves";
            this.Récoltes.UseVisualStyleBackColor = true;
            // 
            // cellarManagement1
            // 
            this.cellarManagement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellarManagement1.Location = new System.Drawing.Point(3, 3);
            this.cellarManagement1.Name = "cellarManagement1";
            this.cellarManagement1.Size = new System.Drawing.Size(473, 570);
            this.cellarManagement1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.batchManagement1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(479, 576);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Récoltes";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // batchManagement1
            // 
            this.batchManagement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.batchManagement1.Location = new System.Drawing.Point(3, 3);
            this.batchManagement1.Name = "batchManagement1";
            this.batchManagement1.Size = new System.Drawing.Size(473, 570);
            this.batchManagement1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.userManagement1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(479, 576);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Utilisateurs";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // userManagement1
            // 
            this.userManagement1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userManagement1.Location = new System.Drawing.Point(3, 3);
            this.userManagement1.Name = "userManagement1";
            this.userManagement1.Size = new System.Drawing.Size(473, 570);
            this.userManagement1.TabIndex = 0;
            // 
            // Loged
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "Loged";
            this.Size = new System.Drawing.Size(487, 602);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.Récoltes.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage tabPage1;
        public BarrelManagement barrelManagement1;
        private CmdManagement cmdManagement1;
        private System.Windows.Forms.TabPage Récoltes;
        private System.Windows.Forms.TabPage tabPage4;
        private CellarManagement cellarManagement1;
        private BatchManagement batchManagement1;
        private System.Windows.Forms.TabPage tabPage3;
        private OldManagers.UserManagement userManagement1;
    }
}
