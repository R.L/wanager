﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public interface IManagerView
    {
        void LoadItems(List<UserControl> toload);

        void LoadLinks();
    }
}
