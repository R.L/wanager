﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views
{
    public interface IBar<Dbobj> //
        where Dbobj : DBObject //
    {
        void Select(object sender, EventArgs e);

        void Delete(object sender, EventArgs e);

        Dbobj GetItem();

        void SetItem(Dbobj obj);

        void SetManager(GenericManager<Dbobj> man);

        UserControl getUC();
           
        //Dbobj Item { get => GetItem(); set => SetItem(value)  }
    }
}
