﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.views.Managers;
using Wanager.model;

namespace Wanager.views
{
    public partial class BatchManagement : UserControl, IManagerView
    {
        protected BatchManager manager;

        public BatchManagement()
        {
            InitializeComponent();

        }

        public void LoadDb()
        {
            this.barrelslist.SuspendLayout();
            this.manager = new BatchManager(Program.model.Batchs, this);
            this.barrelslist.ResumeLayout();
        }

        public void LoadItems(List<UserControl> toload)
        {
            this.barrelslist.Controls.AddRange(toload.ToArray());
        }

        public void LoadLinks()
        {
            
        }

        private void ReloadItems(object sender, EventArgs e)
        {
            this.manager.LoadItems();
        }

        private void AddItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void UpdItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }
    }
}
