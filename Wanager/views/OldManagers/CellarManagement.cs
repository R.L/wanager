﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.views.Managers;
using Wanager.model;

namespace Wanager.views
{
    public partial class CellarManagement : UserControl, IManagerView
    {
        protected CellarManager manager;

        public CellarManagement()
        {
            InitializeComponent();

        }

        public void LoadDb()
        {
            this.barrelslist.SuspendLayout();
            this.manager = new CellarManager(Program.model.Cellars, this);
            this.barrelslist.ResumeLayout();
        }

        public void LoadItems(List<UserControl> toload)
        {
            this.barrelslist.Controls.AddRange(toload.ToArray());
        }

        public void LoadLinks()
        {
           
        }

        private void AddItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void UpdItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void ReloadItems(object sender, EventArgs e)
        {
            this.manager.LoadItems();
        }
    }
}
