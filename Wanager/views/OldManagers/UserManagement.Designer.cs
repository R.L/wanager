﻿namespace Wanager.views.OldManagers
{
    partial class UserManagement
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.addmdp = new System.Windows.Forms.MaskedTextBox();
            this.addname = new System.Windows.Forms.MaskedTextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnreload = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.barrelslist = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.addper1 = new System.Windows.Forms.CheckBox();
            this.addper2 = new System.Windows.Forms.CheckBox();
            this.addper3 = new System.Windows.Forms.CheckBox();
            this.addper4 = new System.Windows.Forms.CheckBox();
            this.addper5 = new System.Windows.Forms.CheckBox();
            this.updper5 = new System.Windows.Forms.CheckBox();
            this.updper4 = new System.Windows.Forms.CheckBox();
            this.updper3 = new System.Windows.Forms.CheckBox();
            this.updper2 = new System.Windows.Forms.CheckBox();
            this.updper1 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.updmdp = new System.Windows.Forms.MaskedTextBox();
            this.updname = new System.Windows.Forms.MaskedTextBox();
            this.updbtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.addper5);
            this.tabPage1.Controls.Add(this.addper4);
            this.tabPage1.Controls.Add(this.addper3);
            this.tabPage1.Controls.Add(this.addper2);
            this.tabPage1.Controls.Add(this.addper1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.addmdp);
            this.tabPage1.Controls.Add(this.addname);
            this.tabPage1.Controls.Add(this.addbtn);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(161, 540);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ajouter";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // addmdp
            // 
            this.addmdp.Location = new System.Drawing.Point(5, 56);
            this.addmdp.Margin = new System.Windows.Forms.Padding(2);
            this.addmdp.Name = "addmdp";
            this.addmdp.Size = new System.Drawing.Size(92, 20);
            this.addmdp.TabIndex = 11;
            // 
            // addname
            // 
            this.addname.Location = new System.Drawing.Point(5, 17);
            this.addname.Margin = new System.Windows.Forms.Padding(2);
            this.addname.Name = "addname";
            this.addname.Size = new System.Drawing.Size(92, 20);
            this.addname.TabIndex = 10;
            // 
            // addbtn
            // 
            this.addbtn.Location = new System.Drawing.Point(5, 209);
            this.addbtn.Margin = new System.Windows.Forms.Padding(2);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(56, 19);
            this.addbtn.TabIndex = 0;
            this.addbtn.Text = "Valider";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.AddItem);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Mot de passe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nom";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.barrelslist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 566);
            this.panel1.TabIndex = 4;
            // 
            // btnreload
            // 
            this.btnreload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnreload.Location = new System.Drawing.Point(446, 0);
            this.btnreload.Margin = new System.Windows.Forms.Padding(2);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(65, 19);
            this.btnreload.TabIndex = 11;
            this.btnreload.Text = "Actualiser";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.ReloadItems);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(169, 566);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.updper5);
            this.tabPage2.Controls.Add(this.updper4);
            this.tabPage2.Controls.Add(this.updper3);
            this.tabPage2.Controls.Add(this.updper2);
            this.tabPage2.Controls.Add(this.updper1);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.updmdp);
            this.tabPage2.Controls.Add(this.updname);
            this.tabPage2.Controls.Add(this.updbtn);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(161, 540);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modifier";
            // 
            // barrelslist
            // 
            this.barrelslist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barrelslist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barrelslist.Location = new System.Drawing.Point(170, 20);
            this.barrelslist.Margin = new System.Windows.Forms.Padding(2);
            this.barrelslist.Name = "barrelslist";
            this.barrelslist.Size = new System.Drawing.Size(343, 546);
            this.barrelslist.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 78);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Permissions";
            // 
            // addper1
            // 
            this.addper1.AutoSize = true;
            this.addper1.Location = new System.Drawing.Point(7, 95);
            this.addper1.Name = "addper1";
            this.addper1.Size = new System.Drawing.Size(46, 17);
            this.addper1.TabIndex = 13;
            this.addper1.Text = "Fûts";
            this.addper1.UseVisualStyleBackColor = true;
            // 
            // addper2
            // 
            this.addper2.AutoSize = true;
            this.addper2.Location = new System.Drawing.Point(7, 118);
            this.addper2.Name = "addper2";
            this.addper2.Size = new System.Drawing.Size(68, 17);
            this.addper2.TabIndex = 14;
            this.addper2.Text = "Recoltes";
            this.addper2.UseVisualStyleBackColor = true;
            // 
            // addper3
            // 
            this.addper3.AutoSize = true;
            this.addper3.Location = new System.Drawing.Point(7, 141);
            this.addper3.Name = "addper3";
            this.addper3.Size = new System.Drawing.Size(56, 17);
            this.addper3.TabIndex = 15;
            this.addper3.Text = "Caves";
            this.addper3.UseVisualStyleBackColor = true;
            // 
            // addper4
            // 
            this.addper4.AutoSize = true;
            this.addper4.Location = new System.Drawing.Point(7, 164);
            this.addper4.Name = "addper4";
            this.addper4.Size = new System.Drawing.Size(84, 17);
            this.addper4.TabIndex = 16;
            this.addper4.Text = "Commandes";
            this.addper4.UseVisualStyleBackColor = true;
            // 
            // addper5
            // 
            this.addper5.AutoSize = true;
            this.addper5.Location = new System.Drawing.Point(7, 187);
            this.addper5.Name = "addper5";
            this.addper5.Size = new System.Drawing.Size(77, 17);
            this.addper5.TabIndex = 17;
            this.addper5.Text = "Utilisateurs";
            this.addper5.UseVisualStyleBackColor = true;
            // 
            // updper5
            // 
            this.updper5.AutoSize = true;
            this.updper5.Location = new System.Drawing.Point(6, 188);
            this.updper5.Name = "updper5";
            this.updper5.Size = new System.Drawing.Size(77, 17);
            this.updper5.TabIndex = 28;
            this.updper5.Text = "Utilisateurs";
            this.updper5.UseVisualStyleBackColor = true;
            // 
            // updper4
            // 
            this.updper4.AutoSize = true;
            this.updper4.Location = new System.Drawing.Point(6, 165);
            this.updper4.Name = "updper4";
            this.updper4.Size = new System.Drawing.Size(84, 17);
            this.updper4.TabIndex = 27;
            this.updper4.Text = "Commandes";
            this.updper4.UseVisualStyleBackColor = true;
            // 
            // updper3
            // 
            this.updper3.AutoSize = true;
            this.updper3.Location = new System.Drawing.Point(6, 142);
            this.updper3.Name = "updper3";
            this.updper3.Size = new System.Drawing.Size(56, 17);
            this.updper3.TabIndex = 26;
            this.updper3.Text = "Caves";
            this.updper3.UseVisualStyleBackColor = true;
            // 
            // updper2
            // 
            this.updper2.AutoSize = true;
            this.updper2.Location = new System.Drawing.Point(6, 119);
            this.updper2.Name = "updper2";
            this.updper2.Size = new System.Drawing.Size(68, 17);
            this.updper2.TabIndex = 25;
            this.updper2.Text = "Recoltes";
            this.updper2.UseVisualStyleBackColor = true;
            // 
            // updper1
            // 
            this.updper1.AutoSize = true;
            this.updper1.Location = new System.Drawing.Point(6, 96);
            this.updper1.Name = "updper1";
            this.updper1.Size = new System.Drawing.Size(46, 17);
            this.updper1.TabIndex = 24;
            this.updper1.Text = "Fûts";
            this.updper1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Permissions";
            // 
            // updmdp
            // 
            this.updmdp.Location = new System.Drawing.Point(4, 57);
            this.updmdp.Margin = new System.Windows.Forms.Padding(2);
            this.updmdp.Name = "updmdp";
            this.updmdp.Size = new System.Drawing.Size(92, 20);
            this.updmdp.TabIndex = 22;
            // 
            // updname
            // 
            this.updname.Location = new System.Drawing.Point(4, 18);
            this.updname.Margin = new System.Windows.Forms.Padding(2);
            this.updname.Name = "updname";
            this.updname.Size = new System.Drawing.Size(92, 20);
            this.updname.TabIndex = 21;
            // 
            // updbtn
            // 
            this.updbtn.Location = new System.Drawing.Point(4, 210);
            this.updbtn.Margin = new System.Windows.Forms.Padding(2);
            this.updbtn.Name = "updbtn";
            this.updbtn.Size = new System.Drawing.Size(56, 19);
            this.updbtn.TabIndex = 18;
            this.updbtn.Text = "Valider";
            this.updbtn.UseVisualStyleBackColor = true;
            this.updbtn.Click += new System.EventHandler(this.UpdItem);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 42);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Mot de passe";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Nom";
            // 
            // UserManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "UserManagement";
            this.Size = new System.Drawing.Size(513, 566);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnreload;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.FlowLayoutPanel barrelslist;
        private System.Windows.Forms.MaskedTextBox addmdp;
        private System.Windows.Forms.MaskedTextBox addname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox addper2;
        private System.Windows.Forms.CheckBox addper1;
        private System.Windows.Forms.CheckBox addper5;
        private System.Windows.Forms.CheckBox addper4;
        private System.Windows.Forms.CheckBox addper3;
        private System.Windows.Forms.CheckBox updper5;
        private System.Windows.Forms.CheckBox updper4;
        private System.Windows.Forms.CheckBox updper3;
        private System.Windows.Forms.CheckBox updper2;
        private System.Windows.Forms.CheckBox updper1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox updmdp;
        private System.Windows.Forms.MaskedTextBox updname;
        private System.Windows.Forms.Button updbtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
