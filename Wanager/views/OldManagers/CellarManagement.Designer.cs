﻿namespace Wanager.views
{
    partial class CellarManagement
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.addpos = new System.Windows.Forms.Label();
            this.addstat = new System.Windows.Forms.MaskedTextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.barrelslist = new System.Windows.Forms.FlowLayoutPanel();
            this.btnreload = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.updstat = new System.Windows.Forms.MaskedTextBox();
            this.updbtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.addmax = new System.Windows.Forms.MaskedTextBox();
            this.addname = new System.Windows.Forms.MaskedTextBox();
            this.updname = new System.Windows.Forms.MaskedTextBox();
            this.updmax = new System.Windows.Forms.MaskedTextBox();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addpos
            // 
            this.addpos.AutoSize = true;
            this.addpos.Location = new System.Drawing.Point(2, 79);
            this.addpos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addpos.Name = "addpos";
            this.addpos.Size = new System.Drawing.Size(26, 13);
            this.addpos.TabIndex = 10;
            this.addpos.Text = "Etat";
            // 
            // addstat
            // 
            this.addstat.Location = new System.Drawing.Point(4, 95);
            this.addstat.Margin = new System.Windows.Forms.Padding(2);
            this.addstat.Name = "addstat";
            this.addstat.Size = new System.Drawing.Size(76, 20);
            this.addstat.TabIndex = 9;
            // 
            // addbtn
            // 
            this.addbtn.Location = new System.Drawing.Point(4, 119);
            this.addbtn.Margin = new System.Windows.Forms.Padding(2);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(56, 19);
            this.addbtn.TabIndex = 0;
            this.addbtn.Text = "Valider";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.AddItem);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Capacité max";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(161, 691);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Autre";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nom";
            // 
            // barrelslist
            // 
            this.barrelslist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barrelslist.Location = new System.Drawing.Point(170, 20);
            this.barrelslist.Margin = new System.Windows.Forms.Padding(2);
            this.barrelslist.Name = "barrelslist";
            this.barrelslist.Size = new System.Drawing.Size(486, 697);
            this.barrelslist.TabIndex = 1;
            // 
            // btnreload
            // 
            this.btnreload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnreload.Location = new System.Drawing.Point(589, 0);
            this.btnreload.Margin = new System.Windows.Forms.Padding(2);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(65, 19);
            this.btnreload.TabIndex = 11;
            this.btnreload.Text = "Actualiser";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.ReloadItems);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.updmax);
            this.tabPage2.Controls.Add(this.updname);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.updstat);
            this.tabPage2.Controls.Add(this.updbtn);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(161, 691);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modifier";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Etat";
            // 
            // updstat
            // 
            this.updstat.Location = new System.Drawing.Point(4, 95);
            this.updstat.Margin = new System.Windows.Forms.Padding(2);
            this.updstat.Name = "updstat";
            this.updstat.Size = new System.Drawing.Size(76, 20);
            this.updstat.TabIndex = 16;
            // 
            // updbtn
            // 
            this.updbtn.Location = new System.Drawing.Point(4, 119);
            this.updbtn.Margin = new System.Windows.Forms.Padding(2);
            this.updbtn.Name = "updbtn";
            this.updbtn.Size = new System.Drawing.Size(56, 19);
            this.updbtn.TabIndex = 11;
            this.updbtn.Text = "Valider";
            this.updbtn.UseVisualStyleBackColor = true;
            this.updbtn.Click += new System.EventHandler(this.UpdItem);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 41);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Capacité max";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 2);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Nom";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.barrelslist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 717);
            this.panel1.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(169, 717);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.addname);
            this.tabPage1.Controls.Add(this.addmax);
            this.tabPage1.Controls.Add(this.addpos);
            this.tabPage1.Controls.Add(this.addstat);
            this.tabPage1.Controls.Add(this.addbtn);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(161, 691);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ajouter";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // addmax
            // 
            this.addmax.Location = new System.Drawing.Point(4, 56);
            this.addmax.Margin = new System.Windows.Forms.Padding(2);
            this.addmax.Name = "addmax";
            this.addmax.Size = new System.Drawing.Size(76, 20);
            this.addmax.TabIndex = 11;
            // 
            // addname
            // 
            this.addname.Location = new System.Drawing.Point(5, 19);
            this.addname.Margin = new System.Windows.Forms.Padding(2);
            this.addname.Name = "addname";
            this.addname.Size = new System.Drawing.Size(76, 20);
            this.addname.TabIndex = 12;
            // 
            // updname
            // 
            this.updname.Location = new System.Drawing.Point(5, 17);
            this.updname.Margin = new System.Windows.Forms.Padding(2);
            this.updname.Name = "updname";
            this.updname.Size = new System.Drawing.Size(76, 20);
            this.updname.TabIndex = 18;
            // 
            // updmax
            // 
            this.updmax.Location = new System.Drawing.Point(5, 56);
            this.updmax.Margin = new System.Windows.Forms.Padding(2);
            this.updmax.Name = "updmax";
            this.updmax.Size = new System.Drawing.Size(76, 20);
            this.updmax.TabIndex = 19;
            // 
            // CellarManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "CellarManagement";
            this.Size = new System.Drawing.Size(656, 717);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label addpos;
        private System.Windows.Forms.MaskedTextBox addstat;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.FlowLayoutPanel barrelslist;
        private System.Windows.Forms.Button btnreload;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox updstat;
        private System.Windows.Forms.Button updbtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox updmax;
        private System.Windows.Forms.MaskedTextBox updname;
        private System.Windows.Forms.MaskedTextBox addname;
        private System.Windows.Forms.MaskedTextBox addmax;
    }
}
