﻿namespace Wanager.views {
    partial class CmdManagement {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.barrelslist = new System.Windows.Forms.FlowLayoutPanel();
            this.btnreload = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.antab = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.annwhy = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.anncmd = new System.Windows.Forms.ComboBox();
            this.addcmdtab = new System.Windows.Forms.TabPage();
            this.addfname = new System.Windows.Forms.MaskedTextBox();
            this.addfill = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.addlname = new System.Windows.Forms.MaskedTextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.addbatch = new System.Windows.Forms.ComboBox();
            this.addprice = new System.Windows.Forms.MaskedTextBox();
            this.addaddr = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.addsale = new System.Windows.Forms.MaskedTextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.antab.SuspendLayout();
            this.addcmdtab.SuspendLayout();
            this.SuspendLayout();
            // 
            // barrelslist
            // 
            this.barrelslist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barrelslist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barrelslist.Location = new System.Drawing.Point(170, 20);
            this.barrelslist.Margin = new System.Windows.Forms.Padding(2);
            this.barrelslist.Name = "barrelslist";
            this.barrelslist.Size = new System.Drawing.Size(428, 329);
            this.barrelslist.TabIndex = 1;
            // 
            // btnreload
            // 
            this.btnreload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnreload.Location = new System.Drawing.Point(533, 0);
            this.btnreload.Margin = new System.Windows.Forms.Padding(2);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(65, 19);
            this.btnreload.TabIndex = 11;
            this.btnreload.Text = "Actualiser";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.ReloadItems);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.barrelslist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 349);
            this.panel1.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.antab);
            this.tabControl1.Controls.Add(this.addcmdtab);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(169, 349);
            this.tabControl1.TabIndex = 0;
            // 
            // antab
            // 
            this.antab.Controls.Add(this.label3);
            this.antab.Controls.Add(this.label2);
            this.antab.Controls.Add(this.annwhy);
            this.antab.Controls.Add(this.label1);
            this.antab.Controls.Add(this.anncmd);
            this.antab.Location = new System.Drawing.Point(4, 22);
            this.antab.Name = "antab";
            this.antab.Padding = new System.Windows.Forms.Padding(3);
            this.antab.Size = new System.Drawing.Size(161, 323);
            this.antab.TabIndex = 3;
            this.antab.Text = "Annuler";
            this.antab.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(6, 294);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "/!\\ Etes vous sur?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Raison";
            // 
            // annwhy
            // 
            this.annwhy.Location = new System.Drawing.Point(7, 70);
            this.annwhy.Multiline = true;
            this.annwhy.Name = "annwhy";
            this.annwhy.Size = new System.Drawing.Size(147, 221);
            this.annwhy.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Commande";
            // 
            // anncmd
            // 
            this.anncmd.FormattingEnabled = true;
            this.anncmd.Location = new System.Drawing.Point(7, 31);
            this.anncmd.Margin = new System.Windows.Forms.Padding(2);
            this.anncmd.Name = "anncmd";
            this.anncmd.Size = new System.Drawing.Size(92, 21);
            this.anncmd.TabIndex = 5;
            // 
            // addcmdtab
            // 
            this.addcmdtab.Controls.Add(this.label9);
            this.addcmdtab.Controls.Add(this.addsale);
            this.addcmdtab.Controls.Add(this.addfname);
            this.addcmdtab.Controls.Add(this.addfill);
            this.addcmdtab.Controls.Add(this.label8);
            this.addcmdtab.Controls.Add(this.label111);
            this.addcmdtab.Controls.Add(this.addlname);
            this.addcmdtab.Controls.Add(this.addbtn);
            this.addcmdtab.Controls.Add(this.label4);
            this.addcmdtab.Controls.Add(this.label5);
            this.addcmdtab.Controls.Add(this.label6);
            this.addcmdtab.Controls.Add(this.label7);
            this.addcmdtab.Controls.Add(this.addbatch);
            this.addcmdtab.Controls.Add(this.addprice);
            this.addcmdtab.Controls.Add(this.addaddr);
            this.addcmdtab.Location = new System.Drawing.Point(4, 22);
            this.addcmdtab.Name = "addcmdtab";
            this.addcmdtab.Padding = new System.Windows.Forms.Padding(3);
            this.addcmdtab.Size = new System.Drawing.Size(161, 323);
            this.addcmdtab.TabIndex = 4;
            this.addcmdtab.Text = "Commander";
            this.addcmdtab.UseVisualStyleBackColor = true;
            // 
            // addfname
            // 
            this.addfname.Location = new System.Drawing.Point(7, 57);
            this.addfname.Margin = new System.Windows.Forms.Padding(2);
            this.addfname.Name = "addfname";
            this.addfname.Size = new System.Drawing.Size(149, 20);
            this.addfname.TabIndex = 24;
            // 
            // addfill
            // 
            this.addfill.Location = new System.Drawing.Point(8, 245);
            this.addfill.Margin = new System.Windows.Forms.Padding(2);
            this.addfill.Name = "addfill";
            this.addfill.Size = new System.Drawing.Size(56, 20);
            this.addfill.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 230);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Quantité";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(5, 80);
            this.label111.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(43, 13);
            this.label111.TabIndex = 21;
            this.label111.Text = "Prenom";
            // 
            // addlname
            // 
            this.addlname.Location = new System.Drawing.Point(7, 96);
            this.addlname.Margin = new System.Windows.Forms.Padding(2);
            this.addlname.Name = "addlname";
            this.addlname.Size = new System.Drawing.Size(149, 20);
            this.addlname.TabIndex = 20;
            // 
            // addbtn
            // 
            this.addbtn.Location = new System.Drawing.Point(8, 269);
            this.addbtn.Margin = new System.Windows.Forms.Padding(2);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(56, 19);
            this.addbtn.TabIndex = 11;
            this.addbtn.Text = "Valider";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.AddItem);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 42);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Nom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 192);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Prix";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Adresse";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 3);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Recolte";
            // 
            // addbatch
            // 
            this.addbatch.FormattingEnabled = true;
            this.addbatch.Location = new System.Drawing.Point(7, 20);
            this.addbatch.Margin = new System.Windows.Forms.Padding(2);
            this.addbatch.Name = "addbatch";
            this.addbatch.Size = new System.Drawing.Size(92, 21);
            this.addbatch.TabIndex = 14;
            // 
            // addprice
            // 
            this.addprice.Location = new System.Drawing.Point(7, 208);
            this.addprice.Margin = new System.Windows.Forms.Padding(2);
            this.addprice.Name = "addprice";
            this.addprice.Size = new System.Drawing.Size(56, 20);
            this.addprice.TabIndex = 13;
            // 
            // addaddr
            // 
            this.addaddr.Location = new System.Drawing.Point(7, 133);
            this.addaddr.Margin = new System.Windows.Forms.Padding(2);
            this.addaddr.Name = "addaddr";
            this.addaddr.Size = new System.Drawing.Size(149, 20);
            this.addaddr.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 155);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Date de RDV";
            // 
            // addsale
            // 
            this.addsale.Location = new System.Drawing.Point(7, 170);
            this.addsale.Margin = new System.Windows.Forms.Padding(2);
            this.addsale.Name = "addsale";
            this.addsale.Size = new System.Drawing.Size(149, 20);
            this.addsale.TabIndex = 25;
            // 
            // CmdManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "CmdManagement";
            this.Size = new System.Drawing.Size(600, 349);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.antab.ResumeLayout(false);
            this.antab.PerformLayout();
            this.addcmdtab.ResumeLayout(false);
            this.addcmdtab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.FlowLayoutPanel barrelslist;
        private System.Windows.Forms.Button btnreload;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage antab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox anncmd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox annwhy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage addcmdtab;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.MaskedTextBox addlname;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox addbatch;
        private System.Windows.Forms.MaskedTextBox addprice;
        private System.Windows.Forms.MaskedTextBox addaddr;
        private System.Windows.Forms.MaskedTextBox addfill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox addfname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox addsale;
    }
}
