﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;

namespace Wanager.views.OldManagers
{
    public partial class UserManagement : UserControl , IManagerView
    {
        private UserManager manager;

        public UserManagement()
        {
            InitializeComponent();
        }

        public void LoadItems(List<UserControl> toload)
        {
            this.barrelslist.SuspendLayout();
            this.manager = new UserManager(Program.model.Users, this);
            this.barrelslist.ResumeLayout();
        }

        public void LoadLinks()
        {
           
        }

        private void ReloadItems(object sender, EventArgs e)
        {
            this.manager.LoadItems();
        }

        private void UpdItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void AddItem(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }
    }
}
