﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.views.Managers;
using Wanager.model;

namespace Wanager.views
{
    public partial class CmdManagement : UserControl, IManagerView
    {
        private CommandManager manager;

        public CmdManagement()
        {
            InitializeComponent();

        }

        public void LoadDb()
        {
            this.barrelslist.SuspendLayout();
            this.manager = new CommandManager(Program.model.Commands, this);
            this.barrelslist.ResumeLayout();
        }

        public void LoadItems(List<UserControl> toload)
        {
            this.barrelslist.Controls.AddRange(toload.ToArray());
        }

        public void LoadLinks()
        {
            this.addbatch.Items.AddRange(Program.model.Batchs.GetItems().ToArray());
            this.anncmd.Items.AddRange(Program.model.Cellars.GetItems().ToArray());
        }

        private void AddItem(object sender, EventArgs e)
        {
            try
            {
                Batch batch = (Batch)this.addbatch.SelectedItem;
                int price = int.Parse(this.addprice.Text);
                String fname = this.addfname.Text;
                String lname = this.addlname.Text;
                String addr = this.addaddr.Text;
                int quant = int.Parse(this.addfill.Text);
                DateTime sell = DateTime.Parse(this.addsale.Text);
                this.manager.AddToDb(new Command(0,batch,price,sell,DateTime.Today,fname,lname));
            }
            catch
            {

            }
        }

        private void ReloadItems(object sender, EventArgs e)
        {
            this.manager.LoadItems();
        }
    }
}
