﻿namespace Wanager.views
{
    partial class BarrelManagement
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.barrelslist = new System.Windows.Forms.FlowLayoutPanel();
            this.btnreload = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.addposlbl = new System.Windows.Forms.Label();
            this.addpos = new System.Windows.Forms.MaskedTextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.addcellarlbl = new System.Windows.Forms.Label();
            this.addcellar = new System.Windows.Forms.ComboBox();
            this.laddfilllbl = new System.Windows.Forms.Label();
            this.addmaxlbl = new System.Windows.Forms.Label();
            this.addbatchlbl = new System.Windows.Forms.Label();
            this.addbatch = new System.Windows.Forms.ComboBox();
            this.addfill = new System.Windows.Forms.MaskedTextBox();
            this.addmax = new System.Windows.Forms.MaskedTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.updposlbl = new System.Windows.Forms.Label();
            this.updpos = new System.Windows.Forms.MaskedTextBox();
            this.updbtn = new System.Windows.Forms.Button();
            this.updcellarlbl = new System.Windows.Forms.Label();
            this.updcellar = new System.Windows.Forms.ComboBox();
            this.updfilllbl = new System.Windows.Forms.Label();
            this.updmaxlbl = new System.Windows.Forms.Label();
            this.updbatchlbl = new System.Windows.Forms.Label();
            this.updbatch = new System.Windows.Forms.ComboBox();
            this.updfill = new System.Windows.Forms.MaskedTextBox();
            this.updmax = new System.Windows.Forms.MaskedTextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // barrelslist
            // 
            this.barrelslist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barrelslist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barrelslist.Location = new System.Drawing.Point(170, 20);
            this.barrelslist.Margin = new System.Windows.Forms.Padding(2);
            this.barrelslist.Name = "barrelslist";
            this.barrelslist.Size = new System.Drawing.Size(357, 505);
            this.barrelslist.TabIndex = 1;
            // 
            // btnreload
            // 
            this.btnreload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnreload.Location = new System.Drawing.Point(460, 0);
            this.btnreload.Margin = new System.Windows.Forms.Padding(2);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(65, 19);
            this.btnreload.TabIndex = 11;
            this.btnreload.Text = "Actualiser";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.Reload);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.barrelslist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 525);
            this.panel1.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(169, 525);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.addposlbl);
            this.tabPage1.Controls.Add(this.addpos);
            this.tabPage1.Controls.Add(this.addbtn);
            this.tabPage1.Controls.Add(this.addcellarlbl);
            this.tabPage1.Controls.Add(this.addcellar);
            this.tabPage1.Controls.Add(this.laddfilllbl);
            this.tabPage1.Controls.Add(this.addmaxlbl);
            this.tabPage1.Controls.Add(this.addbatchlbl);
            this.tabPage1.Controls.Add(this.addbatch);
            this.tabPage1.Controls.Add(this.addfill);
            this.tabPage1.Controls.Add(this.addmax);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(161, 499);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ajouter";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // addposlbl
            // 
            this.addposlbl.AutoSize = true;
            this.addposlbl.Location = new System.Drawing.Point(4, 79);
            this.addposlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addposlbl.Name = "addposlbl";
            this.addposlbl.Size = new System.Drawing.Size(71, 13);
            this.addposlbl.TabIndex = 21;
            this.addposlbl.Text = "Emplacement";
            // 
            // addpos
            // 
            this.addpos.Location = new System.Drawing.Point(6, 95);
            this.addpos.Margin = new System.Windows.Forms.Padding(2);
            this.addpos.Name = "addpos";
            this.addpos.Size = new System.Drawing.Size(92, 20);
            this.addpos.TabIndex = 20;
            // 
            // addbtn
            // 
            this.addbtn.Location = new System.Drawing.Point(6, 191);
            this.addbtn.Margin = new System.Windows.Forms.Padding(2);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(56, 21);
            this.addbtn.TabIndex = 11;
            this.addbtn.Text = "Valider";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.AddItem);
            // 
            // addcellarlbl
            // 
            this.addcellarlbl.AutoSize = true;
            this.addcellarlbl.Location = new System.Drawing.Point(4, 41);
            this.addcellarlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addcellarlbl.Name = "addcellarlbl";
            this.addcellarlbl.Size = new System.Drawing.Size(35, 13);
            this.addcellarlbl.TabIndex = 19;
            this.addcellarlbl.Text = "Cellier";
            // 
            // addcellar
            // 
            this.addcellar.FormattingEnabled = true;
            this.addcellar.Location = new System.Drawing.Point(6, 57);
            this.addcellar.Margin = new System.Windows.Forms.Padding(2);
            this.addcellar.Name = "addcellar";
            this.addcellar.Size = new System.Drawing.Size(92, 21);
            this.addcellar.TabIndex = 18;
            // 
            // laddfilllbl
            // 
            this.laddfilllbl.AutoSize = true;
            this.laddfilllbl.Location = new System.Drawing.Point(4, 152);
            this.laddfilllbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laddfilllbl.Name = "laddfilllbl";
            this.laddfilllbl.Size = new System.Drawing.Size(47, 13);
            this.laddfilllbl.TabIndex = 17;
            this.laddfilllbl.Text = "Quantité";
            // 
            // addmaxlbl
            // 
            this.addmaxlbl.AutoSize = true;
            this.addmaxlbl.Location = new System.Drawing.Point(4, 115);
            this.addmaxlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addmaxlbl.Name = "addmaxlbl";
            this.addmaxlbl.Size = new System.Drawing.Size(69, 13);
            this.addmaxlbl.TabIndex = 16;
            this.addmaxlbl.Text = "Quantité max";
            // 
            // addbatchlbl
            // 
            this.addbatchlbl.AutoSize = true;
            this.addbatchlbl.Location = new System.Drawing.Point(4, 2);
            this.addbatchlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addbatchlbl.Name = "addbatchlbl";
            this.addbatchlbl.Size = new System.Drawing.Size(44, 13);
            this.addbatchlbl.TabIndex = 15;
            this.addbatchlbl.Text = "Recolte";
            // 
            // addbatch
            // 
            this.addbatch.FormattingEnabled = true;
            this.addbatch.Location = new System.Drawing.Point(6, 19);
            this.addbatch.Margin = new System.Windows.Forms.Padding(2);
            this.addbatch.Name = "addbatch";
            this.addbatch.Size = new System.Drawing.Size(92, 21);
            this.addbatch.TabIndex = 14;
            // 
            // addfill
            // 
            this.addfill.Location = new System.Drawing.Point(6, 168);
            this.addfill.Margin = new System.Windows.Forms.Padding(2);
            this.addfill.Name = "addfill";
            this.addfill.Size = new System.Drawing.Size(56, 20);
            this.addfill.TabIndex = 13;
            // 
            // addmax
            // 
            this.addmax.Location = new System.Drawing.Point(6, 132);
            this.addmax.Margin = new System.Windows.Forms.Padding(2);
            this.addmax.Name = "addmax";
            this.addmax.Size = new System.Drawing.Size(56, 20);
            this.addmax.TabIndex = 12;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.updposlbl);
            this.tabPage2.Controls.Add(this.updpos);
            this.tabPage2.Controls.Add(this.updbtn);
            this.tabPage2.Controls.Add(this.updcellarlbl);
            this.tabPage2.Controls.Add(this.updcellar);
            this.tabPage2.Controls.Add(this.updfilllbl);
            this.tabPage2.Controls.Add(this.updmaxlbl);
            this.tabPage2.Controls.Add(this.updbatchlbl);
            this.tabPage2.Controls.Add(this.updbatch);
            this.tabPage2.Controls.Add(this.updfill);
            this.tabPage2.Controls.Add(this.updmax);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(161, 499);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modifier";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // updposlbl
            // 
            this.updposlbl.AutoSize = true;
            this.updposlbl.Location = new System.Drawing.Point(4, 79);
            this.updposlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updposlbl.Name = "updposlbl";
            this.updposlbl.Size = new System.Drawing.Size(71, 13);
            this.updposlbl.TabIndex = 32;
            this.updposlbl.Text = "Emplacement";
            // 
            // updpos
            // 
            this.updpos.Location = new System.Drawing.Point(6, 95);
            this.updpos.Margin = new System.Windows.Forms.Padding(2);
            this.updpos.Name = "updpos";
            this.updpos.Size = new System.Drawing.Size(92, 20);
            this.updpos.TabIndex = 31;
            // 
            // updbtn
            // 
            this.updbtn.Location = new System.Drawing.Point(6, 191);
            this.updbtn.Margin = new System.Windows.Forms.Padding(2);
            this.updbtn.Name = "updbtn";
            this.updbtn.Size = new System.Drawing.Size(56, 21);
            this.updbtn.TabIndex = 22;
            this.updbtn.Text = "Valider";
            this.updbtn.UseVisualStyleBackColor = true;
            this.updbtn.Click += new System.EventHandler(this.UpdItem);
            // 
            // updcellarlbl
            // 
            this.updcellarlbl.AutoSize = true;
            this.updcellarlbl.Location = new System.Drawing.Point(4, 41);
            this.updcellarlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updcellarlbl.Name = "updcellarlbl";
            this.updcellarlbl.Size = new System.Drawing.Size(35, 13);
            this.updcellarlbl.TabIndex = 30;
            this.updcellarlbl.Text = "Cellier";
            // 
            // updcellar
            // 
            this.updcellar.FormattingEnabled = true;
            this.updcellar.Location = new System.Drawing.Point(6, 57);
            this.updcellar.Margin = new System.Windows.Forms.Padding(2);
            this.updcellar.Name = "updcellar";
            this.updcellar.Size = new System.Drawing.Size(92, 21);
            this.updcellar.TabIndex = 29;
            // 
            // updfilllbl
            // 
            this.updfilllbl.AutoSize = true;
            this.updfilllbl.Location = new System.Drawing.Point(4, 152);
            this.updfilllbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updfilllbl.Name = "updfilllbl";
            this.updfilllbl.Size = new System.Drawing.Size(47, 13);
            this.updfilllbl.TabIndex = 28;
            this.updfilllbl.Text = "Quantité";
            // 
            // updmaxlbl
            // 
            this.updmaxlbl.AutoSize = true;
            this.updmaxlbl.Location = new System.Drawing.Point(4, 115);
            this.updmaxlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updmaxlbl.Name = "updmaxlbl";
            this.updmaxlbl.Size = new System.Drawing.Size(69, 13);
            this.updmaxlbl.TabIndex = 27;
            this.updmaxlbl.Text = "Quantité max";
            // 
            // updbatchlbl
            // 
            this.updbatchlbl.AutoSize = true;
            this.updbatchlbl.Location = new System.Drawing.Point(4, 2);
            this.updbatchlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updbatchlbl.Name = "updbatchlbl";
            this.updbatchlbl.Size = new System.Drawing.Size(44, 13);
            this.updbatchlbl.TabIndex = 26;
            this.updbatchlbl.Text = "Recolte";
            // 
            // updbatch
            // 
            this.updbatch.FormattingEnabled = true;
            this.updbatch.Location = new System.Drawing.Point(6, 19);
            this.updbatch.Margin = new System.Windows.Forms.Padding(2);
            this.updbatch.Name = "updbatch";
            this.updbatch.Size = new System.Drawing.Size(92, 21);
            this.updbatch.TabIndex = 25;
            // 
            // updfill
            // 
            this.updfill.Location = new System.Drawing.Point(6, 168);
            this.updfill.Margin = new System.Windows.Forms.Padding(2);
            this.updfill.Name = "updfill";
            this.updfill.Size = new System.Drawing.Size(56, 20);
            this.updfill.TabIndex = 24;
            // 
            // updmax
            // 
            this.updmax.Location = new System.Drawing.Point(6, 132);
            this.updmax.Margin = new System.Windows.Forms.Padding(2);
            this.updmax.Name = "updmax";
            this.updmax.Size = new System.Drawing.Size(56, 20);
            this.updmax.TabIndex = 23;
            // 
            // BarrelManagement
            // 
            this.Controls.Add(this.panel1);
            this.Name = "BarrelManagement";
            this.Size = new System.Drawing.Size(527, 525);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel barrelslist;
        private System.Windows.Forms.Button btnreload;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label addposlbl;
        private System.Windows.Forms.MaskedTextBox addpos;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label addcellarlbl;
        private System.Windows.Forms.ComboBox addcellar;
        private System.Windows.Forms.Label laddfilllbl;
        private System.Windows.Forms.Label addmaxlbl;
        private System.Windows.Forms.Label addbatchlbl;
        private System.Windows.Forms.ComboBox addbatch;
        private System.Windows.Forms.MaskedTextBox addfill;
        private System.Windows.Forms.MaskedTextBox addmax;
        private System.Windows.Forms.Label updposlbl;
        private System.Windows.Forms.MaskedTextBox updpos;
        private System.Windows.Forms.Button updbtn;
        private System.Windows.Forms.Label updcellarlbl;
        private System.Windows.Forms.ComboBox updcellar;
        private System.Windows.Forms.Label updfilllbl;
        private System.Windows.Forms.Label updmaxlbl;
        private System.Windows.Forms.Label updbatchlbl;
        private System.Windows.Forms.ComboBox updbatch;
        private System.Windows.Forms.MaskedTextBox updfill;
        private System.Windows.Forms.MaskedTextBox updmax;
    }
}
