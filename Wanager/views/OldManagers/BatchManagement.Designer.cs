﻿namespace Wanager.views
{
    partial class BatchManagement
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.addpos = new System.Windows.Forms.Label();
            this.addkind = new System.Windows.Forms.MaskedTextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.barrelslist = new System.Windows.Forms.FlowLayoutPanel();
            this.btnreload = new System.Windows.Forms.Button();
            this.addbest = new System.Windows.Forms.MaskedTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.addquali = new System.Windows.Forms.MaskedTextBox();
            this.updquali = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.updkind = new System.Windows.Forms.MaskedTextBox();
            this.updbtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.updbest = new System.Windows.Forms.MaskedTextBox();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addpos
            // 
            this.addpos.AutoSize = true;
            this.addpos.Location = new System.Drawing.Point(1, 40);
            this.addpos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.addpos.Name = "addpos";
            this.addpos.Size = new System.Drawing.Size(31, 13);
            this.addpos.TabIndex = 10;
            this.addpos.Text = "Type";
            // 
            // addkind
            // 
            this.addkind.Location = new System.Drawing.Point(2, 55);
            this.addkind.Margin = new System.Windows.Forms.Padding(2);
            this.addkind.Name = "addkind";
            this.addkind.Size = new System.Drawing.Size(92, 20);
            this.addkind.TabIndex = 9;
            // 
            // addbtn
            // 
            this.addbtn.Location = new System.Drawing.Point(2, 116);
            this.addbtn.Margin = new System.Windows.Forms.Padding(2);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(56, 19);
            this.addbtn.TabIndex = 0;
            this.addbtn.Text = "Valider";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.AddItem);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Qualité";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 77);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Meilleure vente en:";
            // 
            // barrelslist
            // 
            this.barrelslist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barrelslist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barrelslist.Location = new System.Drawing.Point(170, 20);
            this.barrelslist.Margin = new System.Windows.Forms.Padding(2);
            this.barrelslist.Name = "barrelslist";
            this.barrelslist.Size = new System.Drawing.Size(387, 279);
            this.barrelslist.TabIndex = 1;
            // 
            // btnreload
            // 
            this.btnreload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnreload.Location = new System.Drawing.Point(490, 0);
            this.btnreload.Margin = new System.Windows.Forms.Padding(2);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(65, 19);
            this.btnreload.TabIndex = 11;
            this.btnreload.Text = "Actualiser";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.ReloadItems);
            // 
            // addbest
            // 
            this.addbest.Location = new System.Drawing.Point(2, 92);
            this.addbest.Margin = new System.Windows.Forms.Padding(2);
            this.addbest.Name = "addbest";
            this.addbest.Size = new System.Drawing.Size(92, 20);
            this.addbest.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.updquali);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.updkind);
            this.tabPage2.Controls.Add(this.updbtn);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.updbest);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(161, 273);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modifier";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.barrelslist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(557, 299);
            this.panel1.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(169, 299);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.addquali);
            this.tabPage1.Controls.Add(this.addpos);
            this.tabPage1.Controls.Add(this.addkind);
            this.tabPage1.Controls.Add(this.addbtn);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.addbest);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(161, 273);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ajouter";
            // 
            // addquali
            // 
            this.addquali.Location = new System.Drawing.Point(2, 18);
            this.addquali.Margin = new System.Windows.Forms.Padding(2);
            this.addquali.Name = "addquali";
            this.addquali.Size = new System.Drawing.Size(92, 20);
            this.addquali.TabIndex = 11;
            // 
            // updquali
            // 
            this.updquali.Location = new System.Drawing.Point(5, 18);
            this.updquali.Margin = new System.Windows.Forms.Padding(2);
            this.updquali.Name = "updquali";
            this.updquali.Size = new System.Drawing.Size(92, 20);
            this.updquali.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Type";
            // 
            // updkind
            // 
            this.updkind.Location = new System.Drawing.Point(5, 55);
            this.updkind.Margin = new System.Windows.Forms.Padding(2);
            this.updkind.Name = "updkind";
            this.updkind.Size = new System.Drawing.Size(92, 20);
            this.updkind.TabIndex = 16;
            // 
            // updbtn
            // 
            this.updbtn.Location = new System.Drawing.Point(5, 116);
            this.updbtn.Margin = new System.Windows.Forms.Padding(2);
            this.updbtn.Name = "updbtn";
            this.updbtn.Size = new System.Drawing.Size(56, 19);
            this.updbtn.TabIndex = 12;
            this.updbtn.Text = "Valider";
            this.updbtn.UseVisualStyleBackColor = true;
            this.updbtn.Click += new System.EventHandler(this.UpdItem);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Qualité";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 77);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Meilleure vente en:";
            // 
            // updbest
            // 
            this.updbest.Location = new System.Drawing.Point(5, 92);
            this.updbest.Margin = new System.Windows.Forms.Padding(2);
            this.updbest.Name = "updbest";
            this.updbest.Size = new System.Drawing.Size(92, 20);
            this.updbest.TabIndex = 13;
            // 
            // BatchManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "BatchManagement";
            this.Size = new System.Drawing.Size(557, 299);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label addpos;
        private System.Windows.Forms.MaskedTextBox addkind;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.FlowLayoutPanel barrelslist;
        private System.Windows.Forms.Button btnreload;
        private System.Windows.Forms.MaskedTextBox addbest;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.MaskedTextBox addquali;
        private System.Windows.Forms.MaskedTextBox updquali;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox updkind;
        private System.Windows.Forms.Button updbtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox updbest;
    }
}
