﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.views.Managers;
using Wanager.model;

namespace Wanager.views
{
    public partial class BarrelManagement : UserControl ,IManagerView
    {
        protected BarrelManager manager;
        public BarrelManagement()
        {
            InitializeComponent();
        }

        public void LoadDb()
        {
            this.barrelslist.SuspendLayout();
            this.manager = new BarrelManager(Program.model.Barrels, this);
            this.barrelslist.ResumeLayout();
        }

        public void LoadLinks()
        {
            this.addcellar.Items.AddRange(Program.model.Cellars.GetItems().ToArray());
            this.addbatch.Items.AddRange(Program.model.Batchs.GetItems().ToArray());
        }

        public void LoadItems(List<UserControl> toload)
        {
            this.barrelslist.Controls.AddRange(toload.ToArray());
        }

        public void AddItem(object sender, EventArgs e)
        {
            try
            {
                Batch batch = (Batch)this.addbatch.SelectedItem;
                Cellar cellar = (Cellar)this.addcellar.SelectedItem;
                int pos = int.Parse(this.addpos.Text);
                int max = int.Parse(this.addmax.Text);
                int fill = int.Parse(this.addfill.Text);
                this.manager.AddToDb(new Barrel(0,cellar,pos,batch,max,fill));
            }
            catch
            {

            }
        }

        public void UpdItem(object sender, EventArgs e)
        {
            try
            {
                int id = this.manager.Selected.GetItem().Id;
                Batch batch = (Batch)this.updbatch.SelectedItem;
                Cellar cellar = (Cellar)this.updcellar.SelectedItem;
                int pos = int.Parse(this.updpos.Text);
                int max = int.Parse(this.updmax.Text);
                int fill = int.Parse(this.updfill.Text);
                this.manager.AddToDb(new Barrel(id, cellar, pos, batch, max, fill));
            }
            catch
            {

            }
        }

        private void Reload(object sender, EventArgs e)
        {
            this.manager.LoadItems();
        }
    }
}
