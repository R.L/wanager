﻿namespace Wanager {
    partial class formWanager {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formWanager));
            this.loged1 = new Wanager.views.Loged();
            this.login1 = new Wanager.views.Login();
            this.SuspendLayout();
            // 
            // loged1
            // 
            this.loged1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loged1.Location = new System.Drawing.Point(0, 0);
            this.loged1.Margin = new System.Windows.Forms.Padding(4);
            this.loged1.Name = "loged1";
            this.loged1.Size = new System.Drawing.Size(751, 633);
            this.loged1.TabIndex = 1;
            this.loged1.Visible = false;
            this.loged1.Load += new System.EventHandler(this.loged1_Load);
            // 
            // login1
            // 
            this.login1.AccessibleName = "";
            this.login1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.login1.Location = new System.Drawing.Point(0, 0);
            this.login1.Margin = new System.Windows.Forms.Padding(4);
            this.login1.Name = "login1";
            this.login1.Size = new System.Drawing.Size(751, 633);
            this.login1.TabIndex = 0;
            // 
            // formWanager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 633);
            this.Controls.Add(this.loged1);
            this.Controls.Add(this.login1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(399, 398);
            this.Name = "formWanager";
            this.Text = "Wanager";
            this.ResumeLayout(false);

        }

        #endregion

        public Wanager.views.Loged loged1;
        public Wanager.views.Login login1;
    }
}

