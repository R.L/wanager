﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wanager.model;
using Wanager.views;

namespace Wanager {
    public partial class formWanager : Form {
        private List<BarrelBar> bbarlist;

        public formWanager() {
            InitializeComponent();
            this.bbarlist = new List<BarrelBar>();
        }

        private void loged1_Load(object sender, EventArgs e) {

        }

        public void LoginSuccess() {
            this.login1.Visible = false;
            this.loged1.Visible = true;
        }

        public void LoginFail(string result) {
            this.login1.LoginError(result);
        }
    }
}
