﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wanager.model;

namespace Wanager {
    class Controller {
        string curiden,curmdp;

        public bool Login(string iden,string mdp) {
            string result = Program.auth.Login(iden, mdp);

            if (result=="") {
                this.curiden = iden;
                this.curmdp = mdp;
                Program.view.LoginSuccess();
                Program.model = new Model(Program.auth);
                this.LoadManagers();
            } else {
                Program.view.LoginFail(result);
            }
            return false;
        }

        public void LoadManagers() {
            Program.view.loged1.LoadManagers();

        }

        
    }
}
