-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 11 juin 2019 à 21:13
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `wanager`
--

-- --------------------------------------------------------

--
-- Structure de la table `barrel`
--

DROP TABLE IF EXISTS `barrel`;
CREATE TABLE IF NOT EXISTS `barrel` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cave_id` int(255) NOT NULL,
  `pos` int(255) NOT NULL,
  `batch_id` int(255) NOT NULL,
  `max` int(255) NOT NULL,
  `filling` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_batch_id` (`batch_id`) USING BTREE,
  KEY `FK_cellar_id` (`cave_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `barrel`
--

INSERT INTO `barrel` (`id`, `cave_id`, `pos`, `batch_id`, `max`, `filling`) VALUES
(1, 1, 65, 1, 100, 70),
(2, 1, 32, 1, 60, 60),
(5, 1, 2, 2, 40, 39);

--
-- Déclencheurs `barrel`
--
DROP TRIGGER IF EXISTS `barrel_insert_checkfill`;
DELIMITER $$
CREATE TRIGGER `barrel_insert_checkfill` BEFORE INSERT ON `barrel` FOR EACH ROW BEGIN
        IF NEW.filling > NEW.max THEN
    SET NEW
        .filling = NEW.max;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `batch`
--

DROP TABLE IF EXISTS `batch`;
CREATE TABLE IF NOT EXISTS `batch` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `quality` int(255) NOT NULL,
  `kind` varchar(255) NOT NULL,
  `harvest` date NOT NULL,
  `bestsale` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `batch`
--

INSERT INTO `batch` (`id`, `quality`, `kind`, `harvest`, `bestsale`) VALUES
(1, 13, 'bourbon', '2018-12-01', '2020-09-30'),
(2, 3, 'vin rouge', '2018-12-03', '2018-12-31');

-- --------------------------------------------------------

--
-- Structure de la table `cellar`
--

DROP TABLE IF EXISTS `cellar`;
CREATE TABLE IF NOT EXISTS `cellar` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `max` int(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cellar`
--

INSERT INTO `cellar` (`id`, `name`, `max`, `state`) VALUES
(1, 'cave D', 430, 'ok');

-- --------------------------------------------------------

--
-- Structure de la table `command`
--

DROP TABLE IF EXISTS `command`;
CREATE TABLE IF NOT EXISTS `command` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `batch_id` int(255) NOT NULL,
  `price` int(255) NOT NULL,
  `sold` date NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `pname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cmd_batch_id` (`batch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `barrel_per` tinyint(1) NOT NULL,
  `batch_per` tinyint(1) NOT NULL,
  `cellar_per` tinyint(1) NOT NULL,
  `command_per` tinyint(1) NOT NULL,
  `user_per` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `barrel_per`, `batch_per`, `cellar_per`, `command_per`, `user_per`) VALUES
(1, 'root', '', 1, 1, 1, 1, 1),
(2, 'Admin', 'admin', 1, 1, 1, 1, 1),
(3, 'Vendeur', 'vendeur', 1, 0, 0, 1, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
